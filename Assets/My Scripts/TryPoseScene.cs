﻿using Inworld;
using Inworld.Sample.RPM;
using Mediapipe.Unity.Holistic;
using Microsoft.MixedReality.Toolkit.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using TMPro;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace PoseTeacher
{
    // Main script
    public class TryPoseScene : MonoBehaviour
    {
        PoseInputGetter SelfPoseInputGetter;
        public int Action = -1;
        //public GetInferenceFromDanceModel Model;

        // State of Main
        //public int recording_mode = 0; // 0: not recording, 1: recording, 2: playback, 3: load_file, 4: reset_recording
        public bool pauseSelf = false;

        // For fake data when emulating input from file
        private readonly string fake_file = "jsondata/2020_05_27-00_01_59.txt";
        public PoseInputSource SelfPoseInputSource = PoseInputSource.KINECT;

        public PoseVisuallizer3D poseVisuallizer;
        public GameObject mediapipe;

        public List<PoseData> recordedPose;
        public List<Vector4[]> recordedMediapipePose;
        const float unitAround = 100; // in millimeters
        public GameObject thresholdSlider;
        float threshold;
        public GameObject VFXs;
        public bool simpleMoves = true;
        public int emotion = -1;

        public void ChangeCamera(TextMeshPro component)
        {
            Debug.Log("Change camera");

            if (SelfPoseInputSource == PoseInputSource.KINECT)
            {
                SelfPoseInputSource = PoseInputSource.MEDIAPIPE;
                SaveAndDispose();

                component.text = "Change Camera To Azure Kinect (Low performance)";
            }
            else if (SelfPoseInputSource == PoseInputSource.MEDIAPIPE)
            {
                SelfPoseInputSource = PoseInputSource.KINECT;
                SaveAndDispose();

                component.text = "Change Camera To Mediapipe";
            }

            SelfPoseInputGetter = new PoseInputGetter(SelfPoseInputSource) { ReadDataPath = fake_file };
            GetDataFromJSON();
        }

        public void ChangeTypeMoves(TextMeshPro component)
        {
            if (SelfPoseInputSource != PoseInputSource.MEDIAPIPE && SelfPoseInputSource != PoseInputSource.KINECT)
            {
                Debug.Log("Not possible sorry");
                return;
            }

            else
            {
                if (simpleMoves)
                {
                    simpleMoves = false;
                    component.text = "Change type moves to simple\n(with Avatar)";
                }
                else
                {
                    simpleMoves = true;
                    component.text = "Change type moves to custom\n(with environment)";
                }
            }
        }

        public bool HandAboveHead(PoseData CurrentPose)
        {
            return CurrentPose.data[8].Position.y < CurrentPose.data[27].Position.y
                || CurrentPose.data[15].Position.y < CurrentPose.data[27].Position.y;
        }
        public bool HandsElbowsAboveHead(PoseData CurrentPose)
        {
            return CurrentPose.data[8].Position.y < CurrentPose.data[27].Position.y
                && CurrentPose.data[15].Position.y < CurrentPose.data[27].Position.y
                && CurrentPose.data[6].Position.y < CurrentPose.data[27].Position.y
                && CurrentPose.data[13].Position.y < CurrentPose.data[27].Position.y;
        }
        public bool ElbowsAboveHead(PoseData CurrentPose)
        {
            return CurrentPose.data[6].Position.y < CurrentPose.data[27].Position.y
                && CurrentPose.data[13].Position.y < CurrentPose.data[27].Position.y;
        }

        public bool AngryPose(PoseData CurrentPose)
        {
            return CurrentPose.data[6].Position.y < CurrentPose.data[7].Position.x
                && CurrentPose.data[13].Position.y > CurrentPose.data[14].Position.x;
        }

        public bool HandsOnSide(PoseData CurrentPose)
        {
            return (CurrentPose.data[8].Position.x < CurrentPose.data[27].Position.x
                && CurrentPose.data[15].Position.x < CurrentPose.data[27].Position.x)
                || (CurrentPose.data[8].Position.x > CurrentPose.data[27].Position.x
                && CurrentPose.data[15].Position.x > CurrentPose.data[27].Position.x);
        }

        public bool MediapipeHandsWaving(Vector4[] CurrentPose)
        {
            return (CurrentPose[15].y < CurrentPose[0].y && CurrentPose[16].y > CurrentPose[0].y)
                || (CurrentPose[16].y < CurrentPose[0].y && CurrentPose[15].y > CurrentPose[0].y);
        }

        public bool MediapipeAngryPose(Vector4[] CurrentPose)
        {
            return CurrentPose[15].x < CurrentPose[13].x
                && CurrentPose[16].x > CurrentPose[14].x
                && CurrentPose[15].y > CurrentPose[0].y
                && CurrentPose[16].y > CurrentPose[0].y;
        }

        public bool MediapipeElbowsAboveHead(Vector4[] CurrentPose)
        {
            return CurrentPose[13].y < CurrentPose[0].y
                && CurrentPose[14].y < CurrentPose[0].y;
        }

        public bool MediapipeHandsOnSide(Vector4[] CurrentPose)
        {
            return (CurrentPose[15].x < CurrentPose[0].x
                && CurrentPose[16].x < CurrentPose[0].x)
                ||
                (CurrentPose[15].x > CurrentPose[0].x
                && CurrentPose[16].x > CurrentPose[0].x);
        }

        private void GetDataFromJSON()
        {
            switch (SelfPoseInputSource)
            {
                case PoseInputSource.KINECT:
                    if (File.Exists(Application.persistentDataPath + "/recordedMoves.json"))
                    {
                        Positions arrayDataPose;
                        string currentMoves = File.ReadAllText(Application.persistentDataPath + "/recordedMoves.json");
                        arrayDataPose = JsonUtility.FromJson<Positions>(currentMoves);
                        recordedPose = arrayDataPose.poses.ToList();
                    }
                    break;
                case PoseInputSource.MEDIAPIPE:
                    if (File.Exists(Application.persistentDataPath + "/recordedMediapipeMoves.json"))
                    {
                        MediapipePositions arrayDataPose;
                        string currentMoves = File.ReadAllText(Application.persistentDataPath + "/recordedMediapipeMoves.json");
                        arrayDataPose = JsonUtility.FromJson<MediapipePositions>(currentMoves);
                        recordedMediapipePose = MediapipePositions.ConvertDataToMediapipe(arrayDataPose);
                    }
                    else
                    {
                        recordedMediapipePose = new List<Vector4[]>();
                    }

                    mediapipe.SetActive(true);
                    break;
            }
        }

        // Do once on scene startup
        private void Start()
        {
            SelfPoseInputGetter = new PoseInputGetter(SelfPoseInputSource) { ReadDataPath = fake_file };
            SelfPoseInputGetter.loop = true;

            threshold = thresholdSlider.GetComponent<PinchSlider>().SliderValue * 5;

            GetDataFromJSON();
        }

        // Done at each application update
        async void Update()
        {
            CheckKeyInput();

            if (!pauseSelf)
            {
                switch (SelfPoseInputSource)
                {
                    case PoseInputSource.KINECT:
                        if (simpleMoves)
                        {
                            InworldCharacter character = GameObject.Find("kiel_hololens").GetComponent<InworldCharacter>();
                            string charID = GetComponent<CharacterHandler>().GetLiveSessionID(character);
                            if (emotion != 0 && HandAboveHead(SelfPoseInputGetter.GetNextPose()))
                            {
                                emotion = 0;
                                GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Hello, how are you ?");
                                Debug.Log("Greetings");
                                //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(13);

                                await Task.Delay(5000);
                            }
                            if (emotion != 1 && AngryPose(SelfPoseInputGetter.GetNextPose()))
                            {
                                emotion = 1;
                                GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "I am so mad, why are you also mad ?");
                                Debug.Log("Angry");
                                await Task.Delay(5000);
                                //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(3);
                            }
                            if (emotion != 2 && ElbowsAboveHead(SelfPoseInputGetter.GetNextPose()))
                            {
                                emotion = 2;
                                GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Look I am so happy but you look happy too, is there a reason ?");
                                Debug.Log("Happy");
                                await Task.Delay(5000);
                                //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(14);
                            }
                            if (emotion != 3 && HandsOnSide(SelfPoseInputGetter.GetNextPose()))
                            {
                                emotion = 3;
                                GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Oh my god look over there !");
                                Debug.Log("Interested");
                                await Task.Delay(5000);
                                //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(15);
                            }
                        }
                        else
                        {
                            if (recordedPose.Count != 0)
                            {
                                PoseData poseToRecord = SelfPoseInputGetter.GetNextPose();

                                for (int i = 0; i < poseToRecord.data.Length; i++)
                                {
                                    poseToRecord.data[i].Position -= poseToRecord.data[0].Position;
                                    poseToRecord.data[i].Orientation = Quaternion.Inverse(poseToRecord.data[0].Orientation) * poseToRecord.data[i].Orientation;
                                }
                                int index = 0;
                                foreach (PoseData poseData in recordedPose)
                                {
                                    if (poseToRecord.ComparePosition(poseData, threshold * unitAround) >= 0.8)
                                    {
                                        Action = index % VFXs.GetComponent<SelectVFX>().vfx.Length;
                                        Debug.Log("Pose accorded !");
                                        break;
                                    }

                                    /*if (poseToRecord.CompareRotation(poseData, 25.0f) >= 0.8)
                                    {
                                        Action = index % VFXs.GetComponent<SelectVFX>().vfx.Length;
                                        Debug.Log("Pose accorded !");
                                        break;
                                    }*/
                                    else
                                    {
                                        Action = -1;
                                    }
                                    index++;
                                }
                            }
                            else
                            {
                                Action = -1;
                            }

                            // Save position
                            if (Input.GetMouseButtonDown(1))
                            {
                                Debug.Log("Recording position !");
                                PoseData poseToRecord = SelfPoseInputGetter.GetNextPose();

                                for (int i = 0; i < poseToRecord.data.Length; i++)
                                {
                                    poseToRecord.data[i].Position -= poseToRecord.data[0].Position;
                                    poseToRecord.data[i].Orientation = Quaternion.Inverse(poseToRecord.data[0].Orientation) * poseToRecord.data[i].Orientation;
                                }

                                recordedPose.Add(poseToRecord);
                                Debug.Log("Record position !");
                            }

                            // Delete all positions
                            if (Input.GetMouseButtonDown(2))
                            {
                                recordedPose.Clear();
                                Debug.Log("Positions deleted !");
                            }
                        }
                        break;

                    // Case of MEDIAPIPE
                    case PoseInputSource.MEDIAPIPE:
                        if (simpleMoves)
                        {
                            if (mediapipe.GetComponent<HolisticTrackingSolution>().landmarks != null)
                            {
                                InworldCharacter character = GameObject.Find("kiel_hololens").GetComponent<InworldCharacter>();
                                string charID = GetComponent<CharacterHandler>().GetLiveSessionID(character);
                                if (emotion != 0 && MediapipeHandsWaving(mediapipe.GetComponent<HolisticTrackingSolution>().landmarks))
                                {
                                    emotion = 0;
                                    GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Hello, how are you ?");
                                    Debug.Log("Greetings");
                                    //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(13);

                                    await Task.Delay(5000);
                                }
                                if (emotion != 1 && MediapipeAngryPose(mediapipe.GetComponent<HolisticTrackingSolution>().landmarks))
                                {
                                    emotion = 1;
                                    GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "I am so mad, why are you also mad ?");
                                    Debug.Log("Angry");
                                    await Task.Delay(5000);
                                    //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(3);
                                }
                                if (emotion != 2 && MediapipeElbowsAboveHead(mediapipe.GetComponent<HolisticTrackingSolution>().landmarks))
                                {
                                    emotion = 2;
                                    GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Look I am so happy but you look happy too, is there a reason ?");
                                    Debug.Log("Happy");
                                    await Task.Delay(5000);
                                    //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(14);
                                }
                                if (emotion != 3 && MediapipeHandsOnSide(mediapipe.GetComponent<HolisticTrackingSolution>().landmarks))
                                {
                                    emotion = 3;
                                    GameObject.Find("InworldController").GetComponent<InworldController>().SendText(charID, "Oh my god look over there !");
                                    Debug.Log("Interested");
                                    await Task.Delay(5000);
                                    //GameObject.Find("EmoCanvas").GetComponent<EmotionCanvas>().SendGesture(15);
                                }

                            }
                        }
                        else
                        {
                            if (recordedMediapipePose.Count != 0 && mediapipe.GetComponent<HolisticTrackingSolution>().landmarks != null)
                            {
                                PoseData poseToRecord = PoseData.ConvertMediapipeToPose(mediapipe.GetComponent<HolisticTrackingSolution>().landmarks);

                                int index = 0;
                                foreach (PoseData poseData in MediapipePositions.ConvertMediapipeToData(recordedMediapipePose).poses)
                                {
                                    if (poseToRecord.ComparePosition(poseData, threshold * 0.03f) >= 0.8)
                                    {
                                        Action = index % VFXs.GetComponent<SelectVFX>().vfx.Length;

                                        // Not working correctly
                                        Debug.Log("Pose accorded !" + poseData.data[0].Position);
                                        break;
                                    }
                                    else
                                    {
                                        Action = -1;
                                    }
                                    index++;
                                }
                            }
                            else
                            {
                                Action = -1;
                            }

                            // Save position
                            if (Input.GetMouseButtonDown(1))
                            {
                                Debug.Log("Recording position !");
                                Vector4[] poseToRecord = mediapipe.GetComponent<HolisticTrackingSolution>().landmarks;

                                recordedMediapipePose.Add(poseToRecord);
                                Debug.Log("Record position !");
                            }


                            if (Input.GetMouseButtonDown(2))
                            {
                                recordedMediapipePose.Clear();
                                Debug.Log("Positions deleted !");
                            }
                        }
                        break;
                }

            }
        }

        private void SaveAndDispose()
        {
            string jsonData = "";
            switch (SelfPoseInputSource)
            {
                case PoseInputSource.KINECT:
                    Positions arrayDataPose = new Positions();
                    arrayDataPose.poses = recordedPose.ToArray();
                    jsonData = JsonUtility.ToJson(arrayDataPose);

                    File.WriteAllText(Application.persistentDataPath + "/recordedMoves.json", jsonData);
                    break;

                case PoseInputSource.MEDIAPIPE:
                    MediapipePositions arrayMediapipeDataPose = MediapipePositions.ConvertMediapipeToData(recordedMediapipePose);

                    jsonData = JsonUtility.ToJson(arrayMediapipeDataPose);
                    File.WriteAllText(Application.persistentDataPath + "/recordedMediapipeMoves.json", jsonData);
                    break;
            }
            Debug.Log($"{jsonData}");

            SelfPoseInputGetter.Dispose();
        }


        // Actions to do before quitting application
        private void OnApplicationQuit()
        {
            SaveAndDispose();
        }

        // Change recording mode via keyboard input for debugging and to not need menu
        void CheckKeyInput()
        {
            threshold = thresholdSlider.GetComponent<PinchSlider>().SliderValue * 5;
            if (Input.GetKeyDown(KeyCode.P))
            {
                Debug.Log("P - toggle self pause");
                pauseSelf = !pauseSelf;
            }

        }
    }
}