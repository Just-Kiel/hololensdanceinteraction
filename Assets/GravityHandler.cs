using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.VFX;

public class GravityHandler : MonoBehaviour
{
    public GameObject fireworks;
    private void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Collision");

        // Here spawn fireworks at correct
        fireworks.GetComponent<VisualEffect>().Play();
        fireworks.GetComponent<VisualEffect>().SetVector3("Position", new Vector3(GetComponent<Transform>().position.x, -4, GetComponent<Transform>().position.z));

        // Reset Ball
        GetComponent<Transform>().position = new Vector3(0.33f, -0.04f, 0.37f);
        GetComponent<Rigidbody>().useGravity = false;
        GetComponent<Rigidbody>().velocity = Vector3.zero;
    }
}
